#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import urllib2
import os
import urllib
import xbmcaddon
import xbmcvfs
import shutil
import json
import urllib
import xbmc
import os
import re
import urllib2
import xbmcgui
import xbmc
import glob
import sys
import xbmcgui
import ssl
import xbmc
import xbmcaddon
import xbmcplugin
import xbmcgui
import os
import shutil
import time
import urllib2
import urllib

from libs.common import settingsValidated, getIPInfo, isVPNConnected, getVPNProfile, getVPNProfileFriendly
from libs.common import getFriendlyProfileList, connectVPN, disconnectVPN, setVPNState, requestVPNCycle, getFilteredProfileList
from libs.common import getAddonPath, isVPNMonitorRunning, setVPNMonitorState, getVPNMonitorState, wizard
from libs.common import getIconPath
from libs.platform import getPlatform, platforms, getPlatformString
from libs.utility import debugTrace, errorTrace, infoTrace
from libs.vpnproviders import getProfileList, genovpnFiles

from libs.platform import getVPNLogFilePath, fakeConnection, isVPNTaskRunning, stopVPN, startVPN, getAddonPath, getSeparator, getUserDataPath
from libs.platform import getVPNConnectionStatus, connection_status, getPlatform, platforms, writeVPNLog, checkVPNInstall, checkVPNCommand
from libs.utility import debugTrace, infoTrace, errorTrace, ifDebug
from libs.vpnproviders import getVPNLocation, getRegexPattern, getProfileList, provider_display, usesUserKeys, usesSingleKey, gotKeys
from libs.vpnproviders import ovpnFilesAvailable, fixOVPNFiles, getLocationFiles, removeGeneratedFiles, copyKeyAndCert, genovpnFiles
from libs.vpnproviders import usesPassAuth, cleanPassFiles
from libs.ipinfo import getIPInfoFrom, getIPSources
from libs.ganalytics import getIP2Location, sendGoogleAnalytics, sendGoogleAnalyticsConnectedCountry
from libs.Mixpanel import Mixpanel


def defCred():
    addon = xbmcaddon.Addon("service.purevpn.monitor")
    fd = open(getIconPath()+"settings.xml", 'r')
    if fd: 
        debugTrace("settings.xml ::::::::::::::::::::::::::::::: opened")
        fd2 = open(getIconPath()+"settings.xml.bak", 'w')
        vpn_username = addon.getSetting("vpn_username")
        vpn_password = addon.getSetting("vpn_password")
        for line in fd:
            if "id=\"vpn_username\"" in line:
                line = "        <setting label=\"32004\" type=\"text\" id=\"vpn_username\"  enable=\"eq(-3,)\" default=\"" +  vpn_username + "\"/>\n"
            elif "id=\"vpn_password\"" in line:
                line = "        <setting label=\"32005\" type=\"text\" id=\"vpn_password\"  enable=\"eq(-5,)\" option=\"hidden\" default=\"" +  vpn_password + "\"/>\n"
            fd2.write(line)
        fd2.close()
        fd.close()
        #os.remove(getIconPath()+"settings.xml")
        shutil.copy(getIconPath()+"settings.xml.bak", getIconPath()+"settings.xml")
    else:
        debugTrace("settings.xml :::::::::::::::::::::::::::::: unable to opend")

defCred()
