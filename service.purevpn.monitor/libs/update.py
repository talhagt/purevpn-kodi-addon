#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import urllib2
import os
import urllib
import xbmcaddon
import xbmcvfs
import shutil
import json
import urllib
import xbmc
import os
import re
import urllib2
import xbmcgui
import xbmc
import glob
import sys
import xbmcgui
import ssl

from libs.common import settingsValidated, getIPInfo, isVPNConnected, getVPNProfile, getVPNProfileFriendly
from libs.common import getFriendlyProfileList, connectVPN, disconnectVPN, setVPNState, requestVPNCycle, getFilteredProfileList
from libs.common import getAddonPath, isVPNMonitorRunning, setVPNMonitorState, getVPNMonitorState, wizard
from libs.common import getIconPath
from libs.platform import getPlatform, platforms, getPlatformString
from libs.utility import debugTrace, errorTrace, infoTrace
from libs.vpnproviders import getProfileList, genovpnFiles

from libs.platform import getVPNLogFilePath, fakeConnection, isVPNTaskRunning, stopVPN, startVPN, getAddonPath, getSeparator, getUserDataPath
from libs.platform import getVPNConnectionStatus, connection_status, getPlatform, platforms, writeVPNLog, checkVPNInstall, checkVPNCommand
from libs.utility import debugTrace, infoTrace, errorTrace, ifDebug
from libs.vpnproviders import getVPNLocation, getRegexPattern, getProfileList, provider_display, usesUserKeys, usesSingleKey, gotKeys
from libs.vpnproviders import ovpnFilesAvailable, fixOVPNFiles, getLocationFiles, removeGeneratedFiles, copyKeyAndCert, genovpnFiles
from libs.vpnproviders import usesPassAuth, cleanPassFiles
from libs.ipinfo import getIPInfoFrom, getIPSources
from libs.ganalytics import getIP2Location, sendGoogleAnalytics, sendGoogleAnalyticsConnectedCountry
from libs.Mixpanel import Mixpanel

addon = xbmcaddon.Addon("service.purevpn.monitor")
version = addon.getAddonInfo('version')
timestr = time.strftime('%d')
flag = 5

def checkLatestVersion():
    debugTrace('Current version is ' + version)   
    global flag
    url = "http://www.purevpn.com/kodi-repo"
    addon = xbmcaddon.Addon("service.purevpn.monitor")
    
    req = urllib2.Request(url,headers={'User-Agent' : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubuntu/11.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30" , "Content-Type": "application/x-www-form-urlencoded"})

    try:
        response = urllib2.urlopen(req, timeout = 4)
        html = response.read()
        s1 = 'service.purevpn.monitor-'
        s2 = '.zip'
        my_string = html[(html.index(s1)+len(s1)):html.index(s2)]

        if version == my_string:
            debugTrace('no update available')
            flag = 0
        else:
            debugTrace('update available')
            flag = 1
    except:
        pass

def createUpdateChecker():
    global flag
    fname = str(getAddonPath(True, 'libs' + "/updatechecker.txt"))
    if os.path.isfile(fname):
        with open(fname, 'r+') as file:
            i = file.read()
            if i != timestr:
                checkLatestVersion()
                file.seek(0)
                file.truncate()
                file.write(timestr)
               
                debugTrace('file overwritten')

            else:
                debugTrace('No need to check now')
              
                file.close()
           
    else:
        with open(fname, 'w') as file:
            checkLatestVersion()
            file.write(timestr)
            file.close()
      
        debugTrace('file created and written')
        

    if flag == 1:
        debugTrace('yes')
      
        return True
    else:
        debugTrace('no')
      
        return False

createUpdateChecker()
